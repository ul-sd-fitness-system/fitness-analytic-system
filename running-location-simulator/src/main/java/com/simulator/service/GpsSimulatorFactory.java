package com.simulator.service;

import java.util.List;

import com.simulator.model.GpsSimulatorRequest;
import com.simulator.model.Point;
import com.simulator.task.LocationSimulator;

public interface GpsSimulatorFactory {
    LocationSimulator prepareGpsSimulator(GpsSimulatorRequest gpsSimulatorRequest);
    LocationSimulator prepareGpsSimulator(LocationSimulator locationSimulator, List<Point> points);
}
