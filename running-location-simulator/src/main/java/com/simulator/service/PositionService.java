package com.simulator.service;

import com.simulator.model.CurrentPosition;

public interface PositionService {
    void processPositionInfo(long id, CurrentPosition currentPosition, boolean sendPositionsToDistributionService);
}
