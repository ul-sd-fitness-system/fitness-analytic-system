package com.simulator.service;

import com.simulator.model.SimulatorInitLocations;

public interface PathService {
    SimulatorInitLocations loadSimulatorInitLocations();
}
