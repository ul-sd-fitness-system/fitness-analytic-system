package com.updater.web;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import com.updater.model.CurrentPositionDto;

@Controller
public class WebSocketApi {

    @MessageMapping("/sendMessage")
    @SendTo("/topic/locations")
    public CurrentPositionDto sendMessage(CurrentPositionDto message) throws Exception {
        return message;
    }
}
